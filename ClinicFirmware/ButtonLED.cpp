#include "ButtonLED.h"
  void ButtonLED::on() {
    mode = Mode::On;
    set(pin,1);
  }
  void ButtonLED::off() {
    mode = Mode::Off;
    set(pin,0);
  }

  void ButtonLED::blink(unsigned long timeStep) {
    blink(timeStep, -1);
  }
  void ButtonLED::blink(unsigned long timeStep, int number) {
    startTime = millis();
    this->timeStep = timeStep;
    this->number;
    mode = Mode::BlinkOn;
    set(pin,1);
  }
  void ButtonLED::check() {
    switch (mode) {
      case Mode::Off:
        break;
      case Mode::On:
        break;
      case Mode::BlinkOn:
        if (millis() - startTime > timeStep) {
          
          set(pin,0);
          startTime = millis();
          mode = Mode::BlinkOff;
        }
        break;
      case Mode::BlinkOff:
        if (number>0||number==-1)
        {
          if (millis() - startTime > timeStep) {
            if (number!=-1)
            {
              number--;
            }
            
            set(pin,1);
            startTime = millis();
            mode = Mode::BlinkOn;
          }
        }
        else
        {
          set(pin,0);
          mode = Mode::Off;
        }
        
        

        break;
    }
  }
