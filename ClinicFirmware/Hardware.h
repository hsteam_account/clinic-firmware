#pragma once
#include "Settings.h"
#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

void activateRelais(int pin) ;
void deactivateRelais(int pin) ;
void setPWM( int pin,int value);
void setPWMOn( int pin);
void setPWMOff( int pin);
void setPWMAmount( int pin,float value);

void SetupHardware();
void buzzSet(int dutyCycle);
void buzzeron();
void buzzeroff();
bool doorClosed();
void openDoor() ;
void turnOnUVs() ;
void turnOffUVs() ;
void turnOnHeat() ;
void turnOffHeat();
void turnOnMainFan();
void turnOffMainFan() ;
void turnOnOuterFan();

void turnOffOuterFan();
void turnOnHeatFan();
void turnOffHeatFan();
void turnOffEverything();
