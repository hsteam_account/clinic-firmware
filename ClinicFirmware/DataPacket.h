#pragma once
struct DataPacket
{
  Time runTime;
  bool wasAborted;
  double averageVoltage;
  String messages;
  DataPacket(Time runTime, bool wasAborted, double averageVoltage, String messages) {
    this->runTime = runTime;
    this->wasAborted = wasAborted;
    this->averageVoltage = averageVoltage;
    this->messages = messages;
  }
};