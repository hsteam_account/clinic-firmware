
#pragma once
//PWM 0-7 pins (7: UV), 8-11 Led pins (umgekehrte Reihenfolge), 12-15 Lüfter (umgekehrte Reihenfolge)

#define HEAT1 1

#define LED1 8
#define LED2 9
#define LED3 10
#define LED4 11


#define STOP_LED LED1

#define START_BUTTON_LED LED1
#define STOP_BUTTON_LED LED2
#define OPEN_BUTTON_LED LED3



#define FAN1 12
#define FAN2 13
#define FAN3 14
#define FAN4 15

#define HEAT_FAN FAN1
#define OUTER_FAN FAN2
#define MAIN_FAN FAN3



#define UV1 7


#define TEMP 14
#define CURRENT_SENSOR 19
#define BUZZER 23

#define DOORLOCK 27

//Buttons 26,25,32,35,34,39,36

//...
#define START_BUTTON 26
#define STOP_BUTTON 25
#define OPEN_BUTTON 32





#define SECOND 1000
#define MINUTE 60000
#define UV_TIME_SHORT 33*MINUTE
#define UV_TIME_MIDDLE 60*MINUTE
#define UV_TIME_LONG 90*MINUTE


// Hatch 1: 18,17 
// Hatch 2: 16, 4 
#define HATCH_OPEN 18
#define HATCH_CLOSE 17
#define HATCH_TIME 15*SECOND
