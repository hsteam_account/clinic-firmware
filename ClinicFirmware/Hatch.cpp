#include "Hatch.h"
void Hatch::idle() {
  deactivate(openPin);
  deactivate(closePin);
}
void Hatch::open() {
  startTime = millis();
  deactivate(closePin);
  activate(openPin);
}
void Hatch::close() {
  startTime = millis();
  deactivate(openPin);
  activate(closePin);
}
void Hatch::check() {
  switch (mode) {
    case Mode::Idle:
      break;
    case Mode::Opening:
      if (millis() - startTime > HATCH_TIME) {
        deactivate(openPin);
        mode = Mode::Idle;
      }
      break;
    case Mode::Closing:
      if (millis() - startTime > HATCH_TIME) {
        deactivate(closePin);
        mode = Mode::Idle;
      }
      break;
  }
}
