#pragma once
struct Button
{
  uint16_t pin;
  bool justPressed;
  Button(uint16_t pin) {
    this->pin = pin;
    this->justPressed = false;
  }
};
bool getButton(Button button) {
  if (button.pin == -1) {
    return false;
  }
  return digitalRead(button.pin);
}
bool getButtonDown(Button button) {
  if (!button.justPressed)
  {
    return getButton(button);
  }
  return false;
}
void updateButton(Button & button) {
  button.justPressed = getButton(button);
}
