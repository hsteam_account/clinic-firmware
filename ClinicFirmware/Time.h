#pragma once
struct  Time
{
  uint8_t hours;
  uint8_t minutes;
  uint8_t seconds;
  uint8_t milliseconds;
  Time() {
    this->hours = 0;
    this->minutes = 0;
    this->seconds = 0;
    this->milliseconds = 0;

  }
  Time(uint8_t hours, uint8_t minutes, uint8_t seconds, uint8_t milliseconds) {
    this->hours = hours;
    this->minutes = minutes;
    this->seconds = seconds;
    this->milliseconds = milliseconds;
  }
  static Time fromSeconds(unsigned long unmanagedSeconds) {
    auto hours = unmanagedSeconds / 3600;
    auto remainingMinutes = unmanagedSeconds % 3600;
    auto minutes = hours / 60;
    auto seconds = remainingMinutes % 60;

    return Time(hours, minutes, seconds, 0);
  }
  static Time fromMillis(unsigned long millis) {
    Time time = fromSeconds(millis / 1000);
    time.milliseconds = millis % 1000;
    return time;
  }
  String toString() {
    return String(hours) + ":" + String(minutes) + ":" + String(seconds);
  }
};
