#pragma once
#include "Hardware.h"
struct ButtonLED {
  enum class Mode {
    On, Off, BlinkOn, BlinkOff
  };
  Mode mode;
  unsigned long startTime;
  unsigned long timeStep;
  int number;
   int pin;
  void(* set)( int,float );
  ButtonLED( int pin, void(* set)( int, float)) {
    mode = Mode::Off;
    this->pin = pin;
    this->set=set;
  }

  void on();
  void off();
  void blink(unsigned long timeStep) ;
  void blink(unsigned long timeStep, int number);
  void check();
};
