#include "ButtonLED.h"

class Mode
{
private:

public:
    bool blink;
    unsigned long timeStep;
    unsigned long uvTime;

    Mode(bool blink,unsigned long timeStep,unsigned long uvTime){
        this->blink=blink;
        this->timeStep=timeStep;
        this->uvTime=uvTime;
    }
};
