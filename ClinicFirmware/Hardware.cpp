#include "Hardware.h"


Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x58);
int freq = 2000;
int channel =1;
int resolution = 8;

void activateRelais(int pin) {
  if (pin == -1) {
    return;
  }
  else {
    digitalWrite(pin, LOW);
  }
}
void deactivateRelais(int pin) {
  if (pin == -1) {
    return;
  }
  else {
    digitalWrite(pin, HIGH);
  }
}


void setPWM(int pin,int value)
{
  if (pin==-1){
    return;
  }
  pwm.setPWM(pin, 4096,4096-value);
}
void setPWMOn(int pin)
{
  if (pin==-1){
    return;
  }
  pwm.setPWM(pin, 4096,0);

}
void setPWMOff(int pin)
{
  if (pin==-1){
    return;
  }
  pwm.setPWM(pin, 0,4096);

}
void setPWMAmount(int pin,float value)
{
   setPWM(pin, 4096*value);
}

void SetupHardware(){
  pinMode(START_BUTTON, INPUT);
  pinMode(STOP_BUTTON, INPUT);
  pinMode(HEAT1, OUTPUT);
  pinMode(OUTER_FAN, OUTPUT);
  pinMode(HEAT_FAN, OUTPUT);
  pinMode(HATCH_OPEN, OUTPUT);
  pinMode(HATCH_CLOSE, OUTPUT);
  
  ledcSetup(channel, freq, resolution);
  pinMode(BUZZER, OUTPUT);
  ledcAttachPin(BUZZER, channel);
  
  pwm.begin();
  pwm.setPWMFreq(1600);
}
void buzzSet(int dutyCycle){
    ledcWrite(channel, dutyCycle);
}
void buzzeron(){
  buzzSet(125);
}
void buzzeroff(){
  buzzSet(0);
}
bool doorClosed() {
  return true;
}
void openDoor() {
  /*
    activateRelais(DOORLOCK);
    delay(1000);
    deactivateRelais(DOORLOCK);*/
}
void turnOnUVs() {
  Serial.println("UV on");
  setPWMOn(UV1);
}
void turnOffUVs() {
  Serial.println("UV off");
  setPWMOff(UV1);
}
void turnOnHeat() {
  Serial.println("Heat on");
  setPWMOn(HEAT1);
}
void turnOffHeat() {
  Serial.println("Heat off");

    setPWMOff(HEAT1);

}
void turnOnMainFan() {

}
void turnOffMainFan() {

}
void turnOnOuterFan() {
  //activateRelais(OUTER_FAN);
}

void turnOffOuterFan() {
  //deactivateRelais(OUTER_FAN);
}
void turnOnHeatFan() {
  //activateRelais(HEAT_FAN);
}
void turnOffHeatFan() {
  //deactivateRelais(HEAT_FAN);
}
void turnOffEverything() {
  turnOffUVs();
  turnOffHeat();
  turnOffHeatFan();
  turnOffMainFan();
  turnOffOuterFan();
}
