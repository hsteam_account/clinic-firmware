#include "Buzzer.h"
void Buzzer::buzz(){
    mode=Mode::On;
    buzzeron();
}
void Buzzer::buzzStop(){
    mode=Mode::Idle;
    buzzeroff();
}
void Buzzer::buzz(unsigned long timeStep){
        mode=Mode::TimedOn;
        buzzeron();
        this->timeStep=timeStep;
        startTime=millis();
}
void Buzzer::buzz(unsigned long timeStepOn,unsigned long timeStepOff, int times){
    mode=Mode::BlinkOn;
    buzzeron();
    this->timeStepOn=timeStepOn;
    this->timeStepOff=timeStepOff;
    this->times=times;
    startTime=millis();
    this->times--;
}
void Buzzer::check(){
    switch (mode){
        case Mode::Idle:
            break;
        case Mode::On:
            break;
        case Mode::TimedOn:
            if(millis()-startTime>timeStep){
                buzzStop();
                mode=Mode::Idle;
            }
            break;
        case Mode::BlinkOn:
            if(millis()-startTime>timeStepOn){
                buzzeroff();
                mode=Mode::BlinkOff;
                startTime=millis();
            }
            break;
        case Mode::BlinkOff:
            if(times>0){  
                if(millis()-startTime>timeStepOff){
                  times--;
                  buzzeron();
                  mode=Mode::BlinkOn;
                  startTime=millis();
                }
            }
            else{
              buzzeroff();
              mode=Mode::Idle;
            }
            break;
    }   
}