#pragma once

#include <WiFi.h>


#include "Settings.h"
#include "Time.h"
#include "DataPacket.h"
#include "Database.h"
#include "Hardware.h"
#include "Button.h"
#include "ButtonLED.h"
#include "HATCH.h"
#include "Buzzer.h"
#include "Mode.h"
enum class State  {
  Standby, Running
};
enum class RunningState  {
  UV, Circulation, AirOut
};

Button startButton(START_BUTTON);
Button stopButton(STOP_BUTTON);
Button openButton(OPEN_BUTTON);
ButtonLED startLED(START_BUTTON_LED,  setPWMAmount);
ButtonLED stopLED(STOP_BUTTON_LED,  setPWMAmount);
ButtonLED openLED(OPEN_BUTTON_LED,  setPWMAmount);

Hatch hatch(HATCH_OPEN,HATCH_CLOSE ,activateRelais, deactivateRelais);
Buzzer buzzer;


Mode modes[3]={
  Mode(false, 0, UV_TIME_SHORT), 
  Mode(true, .5f, UV_TIME_MIDDLE),
  Mode(true, .2f, UV_TIME_LONG)
};
String modeNames[3] = {"ShortMode", "MiddleMode", "LongMode"};

int modeIndex=0;
int lastModeIndex=0;



String states[2] = {"Standby", "Running"};
State state = State::Standby;
State lastState = State::Standby;




String runningStates[3] = {"UV", "Circulation", "AirOut"};
RunningState runningState = RunningState::UV;
RunningState lastRunningState = RunningState::UV;




unsigned long runningStartTime;

unsigned long uvTime;

unsigned long passedUVTime;
unsigned long circulationTime;  
unsigned long airOutTime;  


void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  SetupHardware();


  turnOffEverything();


  lastWifiCheck = millis();

  //WiFi.begin(ssid, password);

  stopLED.off();
  hatch.idle();
  buzzer.buzz(50, 100, 4);
  for(int i=0; i<16; i++){
    setPWMOff(i);
 }
 startLED.blink(modes[modeIndex].timeStep);

}

void loop() {
  // put your main code here, to run repeatedly:
  switch (state) {
    case State::Standby:
      if (getButtonDown(startButton))
      {
        if (doorClosed())
        {
          uvTime=modes[modeIndex].uvTime;
          UVState();
        }
        else
        {
          openLED.blink(100, 3);
        }
      }
      else if (getButtonDown(stopButton))
      {
        modeIndex=(modeIndex+1)%3;
        if (modes[modeIndex].blink)
        {
          startLED.blink(modes[modeIndex].timeStep);
        }
        else
        {
          startLED.on();
        }

        
      }
      else if (getButtonDown(openButton))
      {
        openDoor();
      }
      break;
    case State::Running:
      
      if (!doorClosed()) {
        Standby();
        openLED.blink(500, 5);
      }
      if (getButtonDown(startButton))
      {
        
      }
      else if (getButtonDown(stopButton))
      {
        if (runningState==RunningState::UV)
        {
          CirculationState();
        }
        
      }
      else if (getButtonDown(openButton))
      {
        openLED.blink(100,3);
        buzzer.buzz(100, 100, 3);
      }
      switch (runningState)
      {
        case RunningState::UV:
          passedUVTime=millis()-runningStartTime;
          if (millis() - runningStartTime > uvTime) {
            CirculationState();
          }
          break;
        case RunningState::Circulation:
          if (millis() - runningStartTime > circulationTime) {
            AirOutState();
          }
          break;
        case RunningState::AirOut:
          if (millis() - runningStartTime > airOutTime) {
            Standby();
          }
          break;
      }

      break;

  }



  updateButton(startButton);
  updateButton(stopButton);
  updateButton(openButton);
  hatch.check();
  buzzer.check();

  startLED.check();
  stopLED.check();
  openLED.check();

  debugPrint();
  lastState = state;
  lastRunningState = runningState;
  lastModeIndex=modeIndex;
  
  delay(100);
  //delay(5000);
  //postData(DataPacket(Time::fromMillis(millis()),false,0, "Test"));
}
void Standby(){
  stopLED.off();
  turnOffEverything();
  hatch.close();
  state = State::Standby;
}
void UVState(){
  startLED.off();
  stopLED.on();
  state = State::Running;
  runningState = RunningState::UV;
  runningStartTime = millis();
  turnOnUVs();
  turnOnHeat();
  hatch.close();
  turnOnMainFan();
  turnOffOuterFan();
  turnOnHeatFan();
}
void CirculationState(){
  circulationTime=passedUVTime/6;
  Serial.print("circulationTime");
  Serial.println(circulationTime/1000);
  stopLED.blink(500);
  turnOffUVs();
  turnOffHeat();
  runningState = RunningState::Circulation;
  runningStartTime = millis();
}
void AirOutState(){
  airOutTime=passedUVTime/6;
    Serial.print("airOutTime");

  Serial.println(airOutTime/1000);
  stopLED.blink(500);
  hatch.open();
  turnOnOuterFan();
  turnOffHeat();
  runningStartTime = millis();
  runningState = RunningState::AirOut;
}
void debugPrint(){

  if(lastModeIndex!=modeIndex){
    Serial.println(modeNames[modeIndex]);   
  }
   if (state != lastState || runningState != lastRunningState) {
    Serial.print(states[(int)state]);
    if (state == State::Running) {
      Serial.print(", ");
      Serial.print(runningStates[(int)runningState]);
    }
    Serial.println();
  }
  if (state == State::Running && millis() - lastPrint >= 1000) {
    lastPrint = millis();
    Serial.println((millis() - runningStartTime) / 1000);
      double current=analogRead(CURRENT_SENSOR);
  Serial.println(current);
  }  
}
