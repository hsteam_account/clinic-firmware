#pragma once
#include "Hardware.h"

struct Buzzer{
    enum class Mode{
      Idle, On, TimedOn, BlinkOn, BlinkOff
    };
    Mode mode;
    unsigned long startTime,timeStep, timeStepOn, timeStepOff;
    int times;
    Buzzer(){
        mode=Mode::Idle;
    }
    void buzz();
    void buzzStop();
    void buzz(unsigned long timeStep);
    void buzz(unsigned long timeStepOn,unsigned long timeStepOff, int times);
    void check();
};
