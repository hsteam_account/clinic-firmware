#pragma once
#include "Hardware.h"
#include "Settings.h"
struct Hatch
{
  enum class Mode{
    Idle, Closing, Opening
  };
  Mode mode;
  unsigned long startTime;
  int openPin;
  int closePin;
  void(* activate)(int);
  void(* deactivate)(int);
  Hatch(unsigned int openPin,int closePin, void(* activate)(int) ,void(* deactivate)(int)){
    mode=Mode::Idle;
    this->openPin=openPin;
    this->closePin=closePin;
    this->activate=activate;
    this->deactivate=deactivate;
  }
  void idle();
  void open();
  void close();
  void check();
};
